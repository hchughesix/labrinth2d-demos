//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos.plinko;

import android.content.Intent;
import android.graphics.*;
import android.hardware.SensorEvent;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.hchughesix.mobile.games.R;
import com.hchughesix.mobile.games.labrinth2d.demos.BoardActivity;
import com.hchughesix.mobile.labrinth2d.Board;
import com.hchughesix.mobile.labrinth2d.Bumper;
import com.hchughesix.mobile.labrinth2d.Hole;
import com.hchughesix.mobile.labrinth2d.Marble;

public class PlinkoActivity extends BoardActivity {
    private static final int STOP_AFTER = 5;
    private static final int START_LINE = 70;

    int[] colors = new int[]{Color.rgb(40, 30, 10), Color.WHITE, Color.YELLOW, Color.rgb(20, 70, 10), Color.rgb(75, 10, 250),
            Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA, Color.RED};
    private final int numberOfBumpers = 15;
    private final int middleBumperThickness = 10;
    final int holeThickness = 35;
    private final int xNumBumpers = 10;
    private final int yNumBumpers = 30;
    private Marble marble;

    @Override
    public Bitmap getBackgroundImage(final int x, final int y) {
        return BitmapFactory.decodeResource(getResources(), R.drawable.plenko);
    }

    @Override
    public void advanceFrame(SensorEvent event) {
        super.advanceFrame(event);
        checkStopHandle(STOP_AFTER, new Intent(PlinkoActivity.this, PlinkoActivity.class));
    }

    @Override
    protected View getView() {
        board = getBoard();
        view = new View(this) {
            final Paint startLinePaint = new Paint();
            @Override
            protected void onDraw(Canvas canvas) {
                board.draw(canvas);
                startLinePaint.setColor(Color.WHITE);
                canvas.drawRect(EDGE_BUMPER_THICKNESS, EDGE_BUMPER_THICKNESS, board.getWidth()-EDGE_BUMPER_THICKNESS, EDGE_BUMPER_THICKNESS + START_LINE, startLinePaint);
                startLinePaint.setColor(Color.RED);
                startLinePaint.setTextSize(35);
                canvas.drawText("Tap Here", (((float)board.getWidth()-EDGE_BUMPER_THICKNESS)/2)-50, 40+((float)EDGE_BUMPER_THICKNESS + START_LINE)/2, startLinePaint);
            }
        };
        LinearLayout.LayoutParams spiralViewParams = new LinearLayout.LayoutParams(board.getWidth(), board.getHeight());
        spiralViewParams.gravity = Gravity.CENTER_HORIZONTAL;
        view.setLayoutParams(spiralViewParams);
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP &&
                        EDGE_BUMPER_THICKNESS + START_LINE > motionEvent.getY()){
                            if((board.getFreeMovings().size() - board.getFreeMovingsInHoles()) >= 1)
                                return true;
                            marble = new Marble.MarbleBuilder(board).setRadius(6).setColor(Color.RED)
                                    .setPoint(new Point((int) motionEvent.getX(), (int) motionEvent.getY()))
                                    .setFriction(.001f).setImpactEnergyLoss(1.75f).setMaxVelocity(10).createMarble();
                            board.addFreeMoving(marble);
                }
                return true;
            }
        });
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        return view;
    }

    @Override
    public String getAppName() {
        return "plinko";
    }

    @Override
    public void init(Board board) {
        int xInterval = (board.getWidth() - (EDGE_BUMPER_THICKNESS * 2)) / xNumBumpers;
        int yInterval = (board.getHeight() - (EDGE_BUMPER_THICKNESS * 2)) / yNumBumpers;
        int bumperOffset = middleBumperThickness / numberOfBumpers;
        for (int i = 0; i <= yNumBumpers; i++) {
            for (int j = 0; j <= xNumBumpers; j++) {
                int xOffset = 0;
                if(i % 2 == 0){
                    xOffset = xInterval/2;
                    if(j == xNumBumpers){continue;}
                }
                if(i == yNumBumpers){
                    if(j <= xNumBumpers-2){
                        xOffset = xInterval/2;
                        int holeOffset = bumperOffset - 3;
                        final RectF rect = new RectF(
                                xOffset + (EDGE_BUMPER_THICKNESS + (j * xInterval)) - (holeOffset * j),
                                (EDGE_BUMPER_THICKNESS + (i * yInterval)) - (bumperOffset * i) - 60 - ACTIONBAROFFSET,
                                xOffset + (EDGE_BUMPER_THICKNESS + (j * xInterval) + holeThickness) - (holeOffset * j),
                                (EDGE_BUMPER_THICKNESS + (i * yInterval) + holeThickness) - (bumperOffset * i)
                        );
                        final int rand = 1 + (int)(Math.random() * ((colors.length - 1) + 1));
                        board.addHole(new Hole(colors[rand-1], Hole.HoleDisplay.POINTS, rand, rect));
                    }
                }else {
                    final int rand = 1 + (int)(Math.random() * ((25 - 1) + 1));
                    int randXOffset = /*j % 2 == 0 ? */xOffset + rand /*: xOffset - rand*/;
//                    int randYOffset = i % 2 == 0 ? xOffset + rand : xOffset - rand;
                    final RectF rect = new RectF(
                            (EDGE_BUMPER_THICKNESS + (j * xInterval)) - (bumperOffset * j) + randXOffset,
                            (EDGE_BUMPER_THICKNESS + (i * yInterval)) - (bumperOffset * i) /*+ randYOffset*/,
                            (EDGE_BUMPER_THICKNESS + (j * xInterval) + middleBumperThickness) - (bumperOffset * j) + randXOffset,
                            (EDGE_BUMPER_THICKNESS + (i * yInterval) + middleBumperThickness) - (bumperOffset * i) /*+ randYOffset*/
                    );
                    if(i != 1 && i != 2){
                        board.addBarrier(new Bumper(Color.BLACK, rect));}
                }
            }
        }
        board.addHole(new Hole(Color.BLUE, Hole.HoleDisplay.NONE, 0, new RectF(EDGE_BUMPER_THICKNESS, board.getHeight()- EDGE_BUMPER_THICKNESS - 20 - ACTIONBAROFFSET, board.getWidth()- EDGE_BUMPER_THICKNESS, board.getHeight())));
    }

}

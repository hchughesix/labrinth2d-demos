//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.*;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.*;
import android.widget.LinearLayout;
import com.hchughesix.mobile.games.R;
import com.hchughesix.mobile.labrinth2d.*;

import java.util.Date;

public abstract class BoardActivity extends Activity implements SensorEventListener {
    private static boolean ABORT_ON_LANDSCAPR_DEFAULT = true;
    protected static int ACTIONBAROFFSET = 100;
    private static final String X_ADJUSTMENT = "x_adjustment";
    private static final String Y_ADJUSTMENT = "y_adjustment";
    protected static final String HIGH_SCORE = "high_score";
    protected static final int EDGE_BUMPER_THICKNESS = 50;

    protected SensorManager sensorManager;
    protected View view;
    protected Board board;
    private int[] adjustment = new int[] {1, 1};
    private boolean stopped = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        switch (getDeviceDefaultOrientation()){
            case Configuration.ORIENTATION_LANDSCAPE:
                if(ABORT_ON_LANDSCAPR_DEFAULT){
                    new AlertDialog.Builder(this)
                            .setMessage("Only portrait orientation default devices supported at this point.  Tablet support coming soon!")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                }
                adjustment = new int[] {1, 1};
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                adjustment = new int[] {-1, 1};
                break;
            default:
                adjustment = new int[] {1, 1};
        }
        SharedPreferences settings = getSharedPreferences(getAppName(), 0);
        adjustment = new int[] {settings.contains(X_ADJUSTMENT) ? settings.getInt(X_ADJUSTMENT, 1) : adjustment[0],
                settings.contains(Y_ADJUSTMENT) ? settings.getInt(Y_ADJUSTMENT, 1) : adjustment[1]};
        sensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
        setContentView(getView());
    }

    public int getDeviceDefaultOrientation() {
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Configuration config = getResources().getConfiguration();
        int rotation = windowManager.getDefaultDisplay().getRotation();
        if (((rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) &&
                config.orientation == Configuration.ORIENTATION_LANDSCAPE)
                || ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) &&
                config.orientation == Configuration.ORIENTATION_PORTRAIT)) {
            return Configuration.ORIENTATION_LANDSCAPE;
        }
        else{
            return Configuration.ORIENTATION_PORTRAIT;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (stopped || !board.isReady() || event.sensor.getType() != Sensor.TYPE_ACCELEROMETER){
            return;
        }
        advanceFrame(event);
        view.invalidate();
    }

    public void advanceFrame(SensorEvent event){
        for(PhysFreeMoving freeMoving : board.getFreeMovings()){
            if(!freeMoving.isReady()){return;}
            boolean inHole = false;                                                //TODO improve.  should be a more elegent logic
            for (Hole hole : board.getHoles()){
                if(hole.getFreeMovings().contains(freeMoving)){
                    inHole = true;
                    break;
                }
            }
            if(!inHole){
                PositionVelocityTransformObj transform = new PositionVelocityTransformObj(freeMoving, board.getBarriers(), board.getHoles());
                final float[] adjustedVelocity = adjust(event);
                transform.apply(adjustedVelocity[0], adjustedVelocity[1]);
                freeMoving.setVel(transform.getVelNew());
                freeMoving.setPoint(transform.getPoint());
            }
        }
    }

    protected float[] adjust(SensorEvent event){
        return new float[]{adjustment[0] * event.values[0], adjustment[1] * event.values[1]};
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences settings = getSharedPreferences(getAppName(), 0);
        SharedPreferences.Editor editor = settings.edit();
        switch (item.getItemId()) {
            case R.id.invert_x:
                adjustment[0] = -1 * adjustment[0];
                editor.putInt(X_ADJUSTMENT, adjustment[0]);
                editor.commit();
                return true;
            case R.id.invert_y:
                adjustment[1] = -1 * adjustment[1];
                editor.putInt(Y_ADJUSTMENT, adjustment[1]);
                editor.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        ;
    }

    public void stop(){
        stopped = true;
        sensorManager.unregisterListener(BoardActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop();
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final long time = new Date().getTime();
                //wait a few sec before ball starts rolling
                do{}while (new Date().getTime() < (time + 1000));
                sensorManager.registerListener(BoardActivity.this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        start();
    }

    protected Board getBoard(){
        if(null == board){
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            board = new Board(getBackgroundImage(size.x, size.y), size.x, size.y);

            board.addBarrier(new Bumper(Color.BLACK, new RectF(0, 0, board.getWidth(), EDGE_BUMPER_THICKNESS)));  //top
            board.addBarrier(new Bumper(Color.BLACK, new RectF(0, 0, EDGE_BUMPER_THICKNESS, board.getHeight()))); //left
            board.addBarrier(new Bumper(Color.BLACK, new RectF(board.getWidth() - EDGE_BUMPER_THICKNESS, 0, board.getWidth(), board.getHeight())));   //right
            board.addBarrier(new Bumper(Color.BLACK, new RectF(0, board.getHeight() - EDGE_BUMPER_THICKNESS, board.getWidth(), board.getHeight()))); //bottom
            init(board);
        }
        return board;
    }

    public abstract void init(final Board board);

    public Bitmap getBackgroundImage(final int x, final int y){
        return BitmapFactory.decodeResource(getResources(), R.drawable.donkey_bro_512);
    }

    protected View getView(){
        board = getBoard();
        view = new View(this) {
            @Override
            protected void onDraw(Canvas canvas) {
                board.draw(canvas);
            }
        };
        LinearLayout.LayoutParams spiralViewParams = new LinearLayout.LayoutParams(board.getWidth(), board.getHeight());
        spiralViewParams.gravity = Gravity.CENTER_HORIZONTAL;
        view.setLayoutParams(spiralViewParams);
        view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        return view;
    }

    protected void checkStopHandle(final int stopAfter, final Intent nextIntent) {
        checkStopHandle(stopAfter, board.getPoints(), nextIntent);
    }

    protected void checkStopHandle(final int stopAfter, final int pointsTotal, final Intent nextIntent) {
        checkStopHandle(stopAfter, pointsTotal, nextIntent, true, "");
    }

    protected void checkStopHandle(final int stopAfter, final int pointsTotal, final Intent nextIntent, final boolean gameOver, final String messagePrepend) {
        if(board.getFreeMovingsInHoles() > stopAfter){
            stop();
            String message = messagePrepend + " " + pointsTotal + " Points.";
            if(gameOver){
                SharedPreferences settings = getSharedPreferences(getAppName(), 0);
                final int alTime = settings.getInt(HIGH_SCORE, 0);
                if(alTime < pointsTotal){
                    message = "New High Score!!! " + message;
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt(HIGH_SCORE, pointsTotal);
                    editor.commit();
                }
                message += " Play again?";
            }
            new AlertDialog.Builder(this)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            board.clearPoints();
                            finish();
                            startActivity(nextIntent);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    public abstract String getAppName();
}

//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.hchughesix.mobile.games.R;
import com.hchughesix.mobile.games.labrinth2d.demos.brickles.BricklesActivity;
import com.hchughesix.mobile.games.labrinth2d.demos.donkey.DonkeyActivity;
import com.hchughesix.mobile.games.labrinth2d.demos.hello.HelloActivity;
import com.hchughesix.mobile.games.labrinth2d.demos.invaders.InvadersActivity;
import com.hchughesix.mobile.games.labrinth2d.demos.labrinth.LabrinthActivity;
import com.hchughesix.mobile.games.labrinth2d.demos.pinball.PinballActivity;
import com.hchughesix.mobile.games.labrinth2d.demos.plinko.PlinkoActivity;

public class HomeActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button helloButton = (Button)findViewById(R.id.hello_button);
        helloButton.setText(R.string.hello);
        helloButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, HelloActivity.class));
            }
        });

        Button bricklesButton = (Button)findViewById(R.id.brickles_button);
        bricklesButton.setText(R.string.brickles);
        bricklesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, BricklesActivity.class));
            }
        });

        Button donkeyButton = (Button)findViewById(R.id.donkey_button);
        donkeyButton.setText(R.string.donkey);
        donkeyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, DonkeyActivity.class));
            }
        });

        Button invadersButton = (Button)findViewById(R.id.invaders_button);
        invadersButton.setText(R.string.invaders);
        invadersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, InvadersActivity.class));
            }
        });

        Button labrinthButton = (Button)findViewById(R.id.labrinth_button);
        labrinthButton.setText(R.string.labrinth);
        labrinthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, LabrinthActivity.class));
            }
        });

        Button pinballButton = (Button)findViewById(R.id.pinball_button);
        pinballButton.setText(R.string.pinball);
        pinballButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, PinballActivity.class));
            }
        });

        Button plinkoButton = (Button)findViewById(R.id.plinko_button);
        plinkoButton.setText(R.string.plinko);
        plinkoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, PlinkoActivity.class));
            }
        });

    }
}

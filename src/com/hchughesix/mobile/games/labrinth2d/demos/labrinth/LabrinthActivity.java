//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos.labrinth;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.SensorEvent;
import android.view.Display;
import com.hchughesix.mobile.games.R;
import com.hchughesix.mobile.games.labrinth2d.demos.BoardActivity;
import com.hchughesix.mobile.labrinth2d.Board;
import com.hchughesix.mobile.labrinth2d.PositionVelocityTransformColor;
import com.hchughesix.mobile.labrinth2d.Marble;

public class LabrinthActivity extends BoardActivity {

    private Marble marble;

    @Override
    public void advanceFrame(SensorEvent event) {
        if(!marble.isReady()){return;}
        PositionVelocityTransformColor transform = new PositionVelocityTransformColor(marble, board.getBitmap(), Color.BLACK, -15597313, -65536);
        final float[] adjustedVelocity = adjust(event);
        transform.apply(adjustedVelocity[0], adjustedVelocity[1]);
        if(transform.isFinished()){
            stop();
            showFinishedDialog("You Win! Play again?");
            return;
        }
        if(transform.isInHole()){
            stop();
            showFinishedDialog("You Lose!  Play again?");
            return;
        }
        marble.setVel(transform.getVelNew());
        marble.setPoint(transform.getPoint());
    }

    private void showFinishedDialog(final String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        startActivity(new Intent(LabrinthActivity.this, LabrinthActivity.class));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();
    }

    @Override
    public Bitmap getBackgroundImage(final int x, final int y) {
        final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.labrinth3);
        return Bitmap.createScaledBitmap(bitmap, x, y, false);
    }

    @Override
    public String getAppName() {
        return "labrinth";
    }

    @Override
    public void init(Board board) {
        marble = new Marble.MarbleBuilder(board)
                .setPoint(new Point(40, 40))
                .setRadius(5).createMarble();
        board.addFreeMoving(marble);
    }

    @Override
    protected Board getBoard(){
        if(null == board){
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            board = new Board(getBackgroundImage(size.x, size.y), size.x, size.y);
            init(board);
        }
        return board;
    }
}

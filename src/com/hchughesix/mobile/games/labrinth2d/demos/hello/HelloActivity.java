//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos.hello;

import com.hchughesix.mobile.labrinth2d.Board;
import com.hchughesix.mobile.labrinth2d.Marble;
import com.hchughesix.mobile.games.labrinth2d.demos.BoardActivity;

public class HelloActivity extends BoardActivity {

    @Override
    public void init(Board board) {
        board.addFreeMoving(new Marble.MarbleBuilder(board).createMarble());
    }

    @Override
    public String getAppName() {
        return "hello";
    }

}
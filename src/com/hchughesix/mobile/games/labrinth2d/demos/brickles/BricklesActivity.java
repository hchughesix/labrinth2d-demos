//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos.brickles;

import android.graphics.Color;
import android.graphics.RectF;
import com.hchughesix.mobile.labrinth2d.Board;
import com.hchughesix.mobile.labrinth2d.Bumper;
import com.hchughesix.mobile.labrinth2d.Hole;
import com.hchughesix.mobile.labrinth2d.Marble;
import com.hchughesix.mobile.games.labrinth2d.demos.BoardActivity;

public class BricklesActivity extends BoardActivity {

    @Override
    public void init(Board board) {
        board.addFreeMoving(new Marble.MarbleBuilder(board).createMarble());
        final RectF rect = new RectF(EDGE_BUMPER_THICKNESS, board.getHeight() - (EDGE_BUMPER_THICKNESS + 10), board.getWidth() - EDGE_BUMPER_THICKNESS, board.getHeight() - EDGE_BUMPER_THICKNESS);
        board.addHole(new Hole(Color.BLUE, Hole.HoleDisplay.NONE, 0, rect));
        board.addBarrier(new Bumper(Color.DKGRAY, new RectF()));
    }

    @Override
    public String getAppName() {
        return "brickles";
    }
}

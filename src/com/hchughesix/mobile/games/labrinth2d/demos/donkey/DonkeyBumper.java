//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos.donkey;

import android.graphics.RectF;
import com.hchughesix.mobile.labrinth2d.Bumper;

public class DonkeyBumper extends Bumper {
    private static final float amount = 1f;

    private Direction direction;
    final int leftWall;
    final int rightWall;

    public DonkeyBumper(final int color, final RectF rect, final Direction direction, final int leftWall, final int rightWall) {
        super(color, rect);
        this.direction = direction;
        this.leftWall = leftWall;
        this.rightWall = rightWall;
    }

    public enum Direction{
        LEFT, RIGHT
    }


    public void advance(){
        switch (direction){
            case LEFT:
                if(rect.left > leftWall){
                    rect.set(rect.left - amount, rect.top, rect.right - amount, rect.bottom);
                }else {
                    direction = Direction.RIGHT;
                    advance();
                }
                break;
            case RIGHT:
                if(rect.right < rightWall){
                    rect.set(rect.left + amount, rect.top, rect.right + amount, rect.bottom);
                }else {
                    direction = Direction.LEFT;
                    advance();
                }
                break;
        }
    }
}

//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.games.labrinth2d.demos.donkey;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.RectF;
import android.hardware.SensorEvent;
import com.google.common.collect.Lists;
import com.hchughesix.mobile.games.labrinth2d.demos.BoardActivity;
import com.hchughesix.mobile.labrinth2d.Board;
import com.hchughesix.mobile.labrinth2d.Hole;
import com.hchughesix.mobile.labrinth2d.Marble;

import java.util.List;

public class DonkeyActivity extends BoardActivity{
    private final String LEVEL = "level";
    private static int LEVELS_TOTAL = 8;
    private final String POINTS_TOTAL = "points_total";
    private final int FRAMES_BETWEEN_MARBLES = 100;
    private final int BUCKET_THICKNESS = 50;
    private final int NUM_BUCKETS = 3;
    private final List<DonkeyBumper> donkeyBumpers = Lists.newArrayList();
    private long frameCount = 0;
    private int level = 0;
    private int pointsTotal;

    @Override
    public void advanceFrame(SensorEvent event) {
        super.advanceFrame(event);
        checkStopHandle(15);
        for(DonkeyBumper bumper : donkeyBumpers){
            bumper.advance();
        }
        if(frameCount == 0 || frameCount % FRAMES_BETWEEN_MARBLES == 0){
            int color = Color.YELLOW;
            int points = 1;
            if(board.getFreeMovings().size() % 10 == 0){
                color = Color.RED;
                points = 5;
            }
            board.addFreeMoving(new Marble.MarbleBuilder(board)
                    .setRadius(10).setColor(color)
                    .setValue(points)
                    .setPoint(new Point(board.getWidth() / 2, EDGE_BUMPER_THICKNESS + 20))
                    .setFriction(.001f)
                    .setImpactEnergyLoss(.4f)
                    .setMaxVelocity(5 * (level < 4 ? ((level + 1)) : 5))
                    .createMarble());
        }
        frameCount++;
    }

    protected void checkStopHandle(int stopAfter) {
        final Intent intent = new Intent(DonkeyActivity.this, DonkeyActivity.class);
        final int newPointsTotal = pointsTotal + board.getPoints();
        intent.putExtra(POINTS_TOTAL, newPointsTotal);
        if (level == LEVELS_TOTAL){
            intent.putExtra(LEVEL, 0);
            checkStopHandle(stopAfter, newPointsTotal, intent);
        }else {
            intent.putExtra(LEVEL, level);
            checkStopHandle(stopAfter, board.getPoints(), intent, false, "Level " + level + " complete. ");
        }
    }

    @Override
    public void init(Board board) {
        if(null != getIntent()){
            level = getIntent().getIntExtra(LEVEL, 0) + 1;
            pointsTotal = getIntent().getIntExtra(POINTS_TOTAL, 0);
        }
        int ySpace = 70;
        int yInterval = (board.getHeight() - (EDGE_BUMPER_THICKNESS * 2)) / level;
        int middleBumperWidth = (board.getWidth()/2) - 150;
        int middleBumperheight = 25;
        for (int i = 0; i <= level; i++) {
            if(i % 2 == 0){
                final DonkeyBumper greenBumper = new DonkeyBumper(Color.GREEN,
                        new RectF(
                                board.getWidth() - EDGE_BUMPER_THICKNESS - middleBumperWidth, ySpace + EDGE_BUMPER_THICKNESS + (i * yInterval),
                                board.getWidth() - EDGE_BUMPER_THICKNESS, ySpace + EDGE_BUMPER_THICKNESS + (i * yInterval) + middleBumperheight
                        ), DonkeyBumper.Direction.LEFT, EDGE_BUMPER_THICKNESS, board.getWidth()-EDGE_BUMPER_THICKNESS);
                donkeyBumpers.add(greenBumper);
                board.addBarrier(
                        greenBumper);
            }else {
                final DonkeyBumper blackBumper = new DonkeyBumper(Color.MAGENTA,
                        new RectF(
                                EDGE_BUMPER_THICKNESS, ySpace + EDGE_BUMPER_THICKNESS + (i * yInterval),
                                EDGE_BUMPER_THICKNESS + middleBumperWidth, ySpace + EDGE_BUMPER_THICKNESS + (i * yInterval) + middleBumperheight
                        ), DonkeyBumper.Direction.RIGHT, EDGE_BUMPER_THICKNESS, board.getWidth()-EDGE_BUMPER_THICKNESS);
                donkeyBumpers.add(blackBumper);
                board.addBarrier(
                        blackBumper);
            }
        }
        int bucketIncrement = (board.getWidth() - (2 * EDGE_BUMPER_THICKNESS))/NUM_BUCKETS;
        for(int i = 1; i <= NUM_BUCKETS; i++){
            final RectF rect = new RectF(
                    (i * bucketIncrement) - (BUCKET_THICKNESS / 2),
                    board.getHeight() - (BUCKET_THICKNESS + EDGE_BUMPER_THICKNESS) - ACTIONBAROFFSET,
                    (i * bucketIncrement) + (BUCKET_THICKNESS / 2),
                    board.getHeight() - EDGE_BUMPER_THICKNESS
            );
            final int points = i == 1 || i == NUM_BUCKETS ? 1 : 3;
            final int color = i == 1 || i == NUM_BUCKETS ? Color.YELLOW : Color.RED;
            if(level == LEVELS_TOTAL && color == Color.YELLOW){
                continue;
            }
            final Hole bucket = new Hole(color, Hole.HoleDisplay.POINTS, points, rect);
            board.addHole(bucket);
        }
        final RectF rect = new RectF(EDGE_BUMPER_THICKNESS, board.getHeight() - (BUCKET_THICKNESS + EDGE_BUMPER_THICKNESS) - ACTIONBAROFFSET + 30, board.getWidth() - EDGE_BUMPER_THICKNESS, board.getHeight() - EDGE_BUMPER_THICKNESS);
        final Hole water = new Hole(Color.BLUE, Hole.HoleDisplay.NONE, 0, rect);
        board.addHole(water);
    }

    @Override
    public String getAppName() {
        return "donkey";
    }
}
